# Sautor Contests

Extensão de concursos para o sistema Sautor.

## Instalação

Instalar via Composer:

````sh
composer require sautor/contests
````

Para correr a migração:
````sh
php artisan migrate
````
