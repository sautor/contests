<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('cts_contests', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->dateTime('open_at')->nullable();
            $table->dateTime('close_at')->nullable();
            $table->dateTime('voting_open_at')->nullable();
            $table->dateTime('voting_close_at')->nullable();
            $table->longText('opening_text')->nullable();
            $table->longText('disclaimer')->nullable();
            $table->longText('thankyou_text')->nullable();
            $table->string('origin_label')->nullable();
            $table->boolean('accept_text')->default(false);
            $table->boolean('accept_images')->default(false);
            $table->boolean('show_at_home')->default(false);
            $table->unsignedInteger('grupo_id')->nullable();
            $table->foreign('grupo_id')->references('id')->on('grupos');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('cts_contests');
    }
};
