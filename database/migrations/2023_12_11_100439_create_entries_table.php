<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Sautor\Contests\Models\Contest;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('cts_entries', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('origin')->nullable();
            $table->longText('content')->nullable();
            $table->longText('notes')->nullable();
            $table->integer('pessoa_id')->unsigned()->nullable();
            $table->foreign('pessoa_id')->references('id')->on('pessoas')->onDelete('cascade');
            $table->foreignIdFor(Contest::class)->nullable()->constrained(table: 'cts_contests');
            $table->ipAddress('ip')->nullable();
            $table->dateTime('validated_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('cts_entries');
    }
};
