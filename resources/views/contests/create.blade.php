@extends('layouts.group-admin')

@section('group-content')
    <header class="group-page__header">
        <h1 class="group-page__header__title">
            Criar concurso
        </h1>
    </header>

    @php($scope = 'new-contest')
    <form action="{{ $grupo->route('contests.contests.store') }}" method="POST" enctype="multipart/form-data" data-vv-scope="{{ $scope }}">
        @csrf

        @include('contests::contests.form')

        <div class="sautor-addon__actions">
            <button type="submit" class="button button--primary">Criar</button>
        </div>
    </form>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('addons/contests/styles.css') }}">
@endpush
