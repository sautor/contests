<div class="s-cts-grid s-cts-grid-cols-1 md:s-cts-grid-cols-2 s-cts-gap-x-8">
    <div class="lg:s-cts-col-span-2">
        <x-forms.input
                name="name"
                label="Nome"
                :value="isset($contest) ? $contest->name : null"
                required
                :scope="$scope"
        />
    </div>

    <x-forms.input name="open_at" label="Abrir inscrições a" :value="isset($contest) ? $contest->open_at : null"
                   type="datetime-local" :scope="$scope"/>
    <x-forms.input name="close_at" label="Fechar inscrições a" :value="isset($contest) ? $contest->close_at : null"
                   type="datetime-local" :scope="$scope"/>

    <x-forms.input name="voting_open_at" label="Abrir votação a" :value="isset($contest) ? $contest->voting_open_at : null"
                   type="datetime-local" :scope="$scope"/>
    <x-forms.input name="voting_close_at" label="Fechar votação a" :value="isset($contest) ? $contest->voting_close_at : null"
                   type="datetime-local" :scope="$scope"/>

    @foreach(['opening_text' => 'Texto de abertura', 'disclaimer' => 'Aviso', 'thankyou_text' => 'Mensagem de agradecimento'] as $key => $label)
        <div class="field">
            <label class="label">{{ $label }}</label>
            <div class="control">
                <small-content-editor name="{{ $key }}"
                                      value="{{ isset($contest) ? $contest->{$key} : null }}"
                                      simple></small-content-editor>
            </div>
        </div>
    @endforeach

    <x-forms.input
            name="origin_label"
            label="Etiqueta do campo de 'origem'"
            :value="isset($contest) ? $contest->origin_label : null"
            :scope="$scope"
    />

    <div>
        <h4 class="title title--xs s-cts-mb-3">Outras opções</h4>
        <x-forms.checkbox name="accept_text" label="Aceitar texto" :value="isset($contest) ? $contest->accept_text : null" :scope="$scope" />
        <x-forms.checkbox name="accept_images" label="Aceitar imagens" :value="isset($contest) ? $contest->accept_images : null" :scope="$scope" />
        <x-forms.checkbox name="show_at_home" label="Mostrar na página inicial" :value="isset($contest) ? $contest->show_at_home : null" :scope="$scope" />
        <x-forms.file name="banner" label="Banner" accept=".png,.jpg,.jpeg"
                      rules="ext:png,jpg,jpeg" :scope="$scope" />
    </div>
</div>

