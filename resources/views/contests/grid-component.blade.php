@unless($contests->isEmpty())
    <section class="s-cts-grid md:s-cts-grid-cols-2 lg:s-cts-grid-cols-3 s-cts-gap-4">
        @foreach($contests as $contest)
            <article class="s-cts-bg-white s-cts-rounded-lg s-cts-shadow s-cts-flex s-cts-flex-col s-cts-overflow-hidden">
                <div
                        class="s-cts-aspect-[2/1] s-cts-bg-cover s-cts-bg-center s-cts-relative @if($contest->getMedia('banner')->isEmpty()) s-cts-bg-gray-200 s-cts-text-gray-400 s-cts-text-6xl s-cts-flex s-cts-justify-center s-cts-items-center @endif"
                        @if($contest->getMedia('banner')->isNotEmpty()) style="background-image: url({{$contest->getFirstMediaUrl('banner')}});" @endif
                >
                    @if($contest->getMedia('banner')->isEmpty())
                        @if(isset($contest->grupo) and ($contest->grupo->icon ?? $contest->grupo->logo))
                            <img src="{{ $contest->grupo->icon ?? $contest->grupo->logo }}" class="s-cts-w-auto s-cts-h-auto s-cts-object-contain s-cts-max-w-[50%] s-cts-max-h-[70%] s-cts-grayscale s-cts-opacity-[15%]">
                        @else
                            <x-icon icon="award" class="s-cts-w-1/3 s-cts-h-3/4 s-cts-fill-current" />
                        @endif
                    @endif
                </div>
                <div class="s-cts-px-4 s-cts-py-3 s-cts-flex-1 s-cts-flex s-cts-flex-col">
                    <h3 class="s-cts-font-headline s-cts-text-xl">
                        <a href="{{ $contest->grupo->route('contests.public.show', $contest) }}" class="s-cts-text-primary-500 hover:s-cts-text-primary-600">{{ $contest->name }}</a>
                    </h3>
                </div>
            </article>
        @endforeach
    </section>
@else
    <section class="sautor-list--empty tags-list--empty">
        {{ empty($empty_message) ? 'Não existem concursos.' : $empty_message }}
    </section>
@endunless
