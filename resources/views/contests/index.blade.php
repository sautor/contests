@extends('layouts.group-admin')

@section('group-content')
    <div class="group-page__header">
        <h1 class="group-page__header__title">
            Concursos
        </h1>

        <div class="group-page__header__actions">
            <a href="{{ $grupo->route('contests.contests.create') }}" class="button button--responsive">
                <span class="far fa-plus"></span>
                Novo concurso
            </a>
        </div>
    </div>

    @include('contests::contests.list-component', ['actions' => fn ($tc) => [
        ['label' => 'Editar', 'icon' => 'fas fa-pencil', 'link' => $tc->grupo->route('contests.contests.edit', $tc)]
    ]])

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('addons/contests/styles.css') }}">
@endpush
