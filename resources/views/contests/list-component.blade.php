@unless($contests->isEmpty())
    <section class="sautor-list sheets-list">
        @foreach($contests as $contest)
            <div class="sautor-list__item">
                <div class="sautor-list__item__logo">
                    <span class="sautor-list__item__logo__icon sautor-list__item__logo__icon--strong">
                        <span class="fad fa-award fa-swap-opacity"></span>
                    </span>
                </div>
                <a href="{{ $contest->grupo->route('contests.contests.show', $contest) }}" class="sautor-list__item__data">
                    <p class="sautor-list__item__name">
                        {{ $contest->name }}
                    </p>
                    <p class="sautor-list__item__meta">
    {{ $contest->entries->count() }} inscrições
    @if($contest->isVotingOpen)
        <span class="badge badge--success">Votação aberta</span>
    @elseif($contest->isVotingClosed)
        <span class="badge badge--danger">Votação fechada</span>
    @elseif($contest->isOpen)
        <span class="badge badge--success">Inscrições abertas</span>
    @elseif($contest->isClosed)
        <span class="badge badge--danger">Inscrições fechadas</span>
    @else
        <span class="badge">Por abrir</span>
    @endif
</p>
</a>

<div class="sautor-list__item__actions">
@unless(empty($actions) or empty($a = $actions($contest)))
    @foreach($a as $action)
        <a title="{{ $action['label'] }}"
           @if(!empty($action['link']))
               href="{{ $action['link'] }}"
           @elseif(!empty($action['modal']))
               href="#" data-toggle="modal" data-target="#{{ $action['modal'] }}"
                @endif
        >
            <span class="{{ $action['icon'] }}"></span>
        </a>
    @endforeach
@endunless
<a title="Ver" href="{{ $contest->grupo->route('contests.contests.show', $contest) }}">
    <span class="far fa-angle-right"></span>
</a>
</div>
</div>
@endforeach
</section>
@else
<section class="sautor-list--empty tags-list--empty">
{{ empty($empty_message) ? 'Não existem concursos.' : $empty_message }}
</section>
@endunless
