@extends('layouts.group-admin')

@section('group-content')
    <div class="group-page__header">
        <h1 class="group-page__header__title">
            {{ $contest->name }}
            <small>{{ $contest->entries_count }} testemunhos</small>
        </h1>

        <div class="group-page__header__actions">
            <a href="{{ $grupo->route('contests.contests.edit', $contest) }}" class="button button--responsive">
                <span class="fas fa-pencil"></span>
                Editar
            </a>
        </div>
    </div>


    @include('contests::entries.list-component')

    {!! $entries->links() !!}

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('addons/contests/styles.css') }}">
@endpush
