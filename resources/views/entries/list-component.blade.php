@unless($entries->isEmpty())
    <section class="sautor-list sheets-list">
        @foreach($entries as $entry)
            <div class="sautor-list__item">
                <div class="sautor-list__item__logo">
                    @if($entry->hasMedia('images'))
                        <img src="{{ $entry->getFirstMediaUrl('images') }}" class="cover">
                    @else
                    <span class="sautor-list__item__logo__icon sautor-list__item__logo__icon--strong">
                        <span class="fad fa-award"></span>
                    </span>
                    @endif
                </div>
                <a href="{{ $entry->contest->grupo->route('contests.entries.show', $entry) }}" class="sautor-list__item__data">
                    <p class="sautor-list__item__name">
                        {{ $entry->name }}
                    </p>
                    <p class="sautor-list__item__meta">
                        {{ $entry->origin }}

                        @if($entry->hasMedia('images'))
                            &middot; <span class="far fa-images"></span> {{ $entry->getMedia('images')->count() }}
                        @endif
                    </p>
                </a>

                <div class="sautor-list__item__actions">
                    @unless(empty($actions) or empty($a = $actions($collection)))
                        @foreach($a as $action)
                            <a title="{{ $action['label'] }}"
                               @if(!empty($action['link']))
                                   href="{{ $action['link'] }}"
                               @elseif(!empty($action['modal']))
                                   href="#" data-toggle="modal" data-target="#{{ $action['modal'] }}"
                                    @endif
                            >
                                <span class="{{ $action['icon'] }}"></span>
                            </a>
                        @endforeach
                    @endunless
                    <a title="Ver" href="{{ $entry->contest->grupo->route('contests.entries.show', $entry) }}">
                        <span class="far fa-angle-right"></span>
                    </a>
                </div>
            </div>
        @endforeach
    </section>
@else
    <section class="sautor-list--empty tags-list--empty">
        {{ empty($empty_message) ? 'Não existem inscrições.' : $empty_message }}
    </section>
@endunless
