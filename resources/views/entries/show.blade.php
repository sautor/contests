@extends('layouts.group-admin')

@section('group-content')
    <div class="group-page__header">
        <h1 class="group-page__header__title">
            Participação #{{ $entry->id }}
            <small>{{ $entry->contest->name }}</small>
        </h1>
    </div>

    <div class="people-show__tab__card">
        <p class="people-show__item">
            <span class="people-show__item__label">Nome</span>
            {{ $entry->name }}
        </p>

        @unless(empty($entry->contest->origin_label))
        <p class="people-show__item">
            <span class="people-show__item__label">{{ $entry->contest->origin_label }}</span>
            {{ $entry->origin }}
        </p>
        @endunless

        <div class="people-show__item">
            <span class="people-show__item__label">Carregado por</span>
            @empty($entry->pessoa)
                <span class="people-show__item__empty">IP: {{ $entry->ip }} (sem sessão iniciada)</span>
            @else
                <x-people.show :person="$entry->pessoa" :meta="'IP:'.$entry->ip" />
            @endempty
        </div>
    </div>

    @unless(empty($entry->content))
        <header class="people-show__tab__header">
            <h2 class="people-show__tab__title">Conteúdo</h2>
        </header>
        <div class="prose s-cts-whitespace-pre-line s-cts-mb-8 s-cts-italic s-cts-font-accent s-cts-text-gray-700">{!! $entry->content !!}</div>
    @endunless

    @unless(empty($entry->notes))
        <header class="people-show__tab__header">
            <h2 class="people-show__tab__title">Notas</h2>
        </header>
        <div class="prose s-cts-whitespace-pre-line s-cts-mb-8 s-cts-italic s-cts-font-accent s-cts-text-gray-700">{!! $entry->notes !!}</div>
    @endunless

    @if($entry->hasMedia('images'))
        <header class="people-show__tab__header">
            <h2 class="people-show__tab__title">Imagens</h2>
        </header>

        <div class="s-cts-grid s-cts-grid-cols-3 md:s-cts-grid-cols-4 lg:s-cts-grid-cols-5 xl:s-cts-grid-cols-6 s-cts-gap-4">
            @foreach($entry->getMedia('images') as $image)
                <a href="{{ $image->getUrl() }}" target="_blank">
                    <img src="{{ $image->getUrl() }}" class="s-cts-w-full s-cts-aspect-square s-cts-rounded s-cts-object-cover s-cts-border s-cts-border-gray-300" >
                </a>
            @endforeach
        </div>
    @endif

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('addons/contests/styles.css') }}">
@endpush
