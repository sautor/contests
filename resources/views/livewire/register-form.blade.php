<div>
    <h1 class="group-page__title">
        Inscrever
        <br /><small>{{ $contest->name }}</small>
    </h1>

    @if ($isSaved)
        <div class="s-cts-max-w-prose s-cts-mx-auto">
            <div class="alert alert--success">
                <p>
                    Inscrição recebida com sucesso!
                </p>
            </div>

            <div class="prose lg:prose-lg empty:s-cts-hidden s-cts-mx-auto s-cts-mt-8">
                {!! $contest->thankyou_text !!}
            </div>
        </div>
    @else
        <div class="max-w-prose mx-auto">

            <x-forms.input name="name" label="Nome" required :options="['wire:model' => 'name']"/>

            @unless(empty($contest->origin_label))
                <x-forms.input name="origin" :label="$contest->origin_label" required
                               :options="['wire:model' => 'origin']"/>
            @endunless

            @if($contest->accept_text)
                <div class="field @if($errors->has('content')) field--invalid @endif">
                    <label for="content" class="label">
                        Conteúdo
                    </label>
                    <div class="s-cts-mt-2 s-cts-grid" x-data="{ content: '' }">
                        <div class="input s-cts-py-2 s-cts-px-3 s-cts-text-base s-cts-whitespace-pre-wrap"
                             style="visibility: hidden; grid-area: 1/1/2/2;" x-html="content">
                        </div>
                        <textarea name="content" id="content" x-model="content" wire:model="content"
                                  class="input s-cts-min-h-[10rem] s-cts-resize-none"
                                  style="grid-area: 1/1/2/2;"></textarea>
                    </div>
                    @if ($errors->has('content'))
                        <span class="field__help field__help--invalid">
                            {{ $errors->first('content') }}
                        </span>
                    @endif
                </div>
            @endif


            @if($contest->accept_images)

                <x-forms.file name="images" label="Imagens" accept="image/*" multiple
                              :options="['wire:model' => 'images']"/>

                <div class="s-cts-grid s-cts-grid-cols-4 s-cts-gap-4">
                    @foreach($images as $image)
                        <img src="{{ $image->temporaryUrl() }}"
                             class="s-cts-w-full s-cts-aspect-square s-cts-rounded s-cts-object-cover s-cts-border s-cts-border-gray-300">
                    @endforeach
                </div>

            @endif


            <div class="prose empty:s-cts-hidden s-cts-mx-auto s-cts-mb-8 empty:s-cts-hidden">
                {!! $contest->disclaimer !!}
            </div>

            <x-forms.checkbox name="consent"
                              :label="'Eu dou o meu consentimento ao uso desta participação por '.$contest->grupo->nome.' de '.config('app.name').'.'"
                              :options="['wire:model' => 'consent']"/>

            <div class="s-cts-flex s-cts-justify-end s-cts-mt-8">
                <button type="button" class="button button--primary" wire:click="save" wire:loading.attr="disabled">
                        <span wire:loading>
                            A enviar...
                        </span>
                    <span wire:loading.remove>
                            Enviar
                        </span>
                </button>
            </div>

        </div>
    @endif

</div>
