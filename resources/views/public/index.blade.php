@php($isSiteGroup = $grupo->site_group && $grupo->is($grupo->site_group))

@extends('layouts.app')

@section('content')
    <div class="group-page__container" >

        <h1 class="group-page__title">
            Concursos
            @unless($isSiteGroup)
                <small class="group-page__title__group">
                    {{ $grupo->nome_curto }}
                </small>
            @endunless
        </h1>

        @if($registration_contests->isNotEmpty())
            <div class="s-cts-mb-8">
                <h2 class="title title--md s-cts-mb-4">Inscrições abertas</h2>
                @include('contests::contests.grid-component', ['contests' => $registration_contests])
            </div>
        @endif

        @if($voting_contests->isNotEmpty())
            <div class="s-cts-mb-8">
                <h2 class="title title--md s-cts-mb-4">Votação aberta</h2>
                @include('contests::contests.grid-component', ['contests' => $voting_contests])
            </div>
        @endif
</div>
@endsection

@push('styles')
<link rel="stylesheet" href="{{ asset('addons/contests/styles.css') }}">
@endpush
