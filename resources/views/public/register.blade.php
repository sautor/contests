@php($isSiteGroup = $grupo->site_group && $grupo->is($grupo->site_group))

@extends('layouts.app')

@section('content')
    @php($hero = $contest->getFirstMediaUrl('banner'))
    @if($hero)
        <div class="hero @if($grupo->site_group) hero--group @endif ">
            <div class="hero__image" style="background-image: url('{{ $hero }}')">
            </div>
        </div>
    @endif

    <div class="group-page__container" >

        <livewire:register-form :contest="$contest" />

    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('addons/contests/styles.css') }}">
@endpush
