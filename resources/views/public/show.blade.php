@php($isSiteGroup = $grupo->site_group && $grupo->is($grupo->site_group))

@extends('layouts.app')

@section('content')
    @php($hero = $contest->getFirstMediaUrl('banner'))
    @if($hero)
        <div class="hero @if($grupo->site_group) hero--group @endif ">
            <div class="hero__image" style="background-image: url('{{ $hero }}')">
            </div>
        </div>
    @endif

    <div class="group-page__container" >

        <h1 class="group-page__title">
            {{ $contest->name }}
        </h1>

        <div class="prose lg:prose-lg empty:s-cts-hidden s-cts-mx-auto s-cts-mb-8">
            {!! $contest->opening_text !!}
        </div>

        @if($contest->isVotingClosed)
            <p class="s-cts-text-center font-bold">
                A votação já terminou.
            </p>
        @elseif($contest->isVotingOpen)
            <div class="s-cts-text-center">
                <a href="#" class="button button--primary button--large">
                    Votar
                </a>
                @unless(empty($contest->voting_close_at))
                    <p class="s-cts-text-sm s-cts-mt-3 font-accent s-cts-text-gray-600">
                        Votação aberta até {{ $contest->voting_close_at->isoFormat('LL') }}
                    </p>
                @endunless
            </div>
        @elseif($contest->isClosed)
            <p class="s-cts-text-center font-bold">
                As inscrições já fecharam.
            </p>
        @elseif($contest->isOpen)
        <div class="s-cts-text-center">
            <a href="{{ $grupo->route('contests.public.register', $contest) }}" class="button button--primary button--large">
                Inscrever
            </a>
            @unless(empty($contest->close_at))
                <p class="s-cts-text-sm s-cts-mt-3 font-accent s-cts-text-gray-600">
                    Inscrições abertas até {{ $contest->close_at->isoFormat('LL') }}
                </p>
            @endunless
        </div>
        @endif

    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('addons/contests/styles.css') }}">
@endpush
