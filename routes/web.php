<?php

use Sautor\Contests\Controllers\ContestController;
use Sautor\Contests\Controllers\EntryController;
use Sautor\Contests\Controllers\PublicController;

Sautor\groupRoute(function () {
    Route::middleware('addon:contests')->name('contests.')->prefix('contests')->group(function () {
        Route::get('/', [PublicController::class, 'index'])->name('public.index');

        Route::resource('contests', ContestController::class)->middleware('auth');

        Route::get('{contest}', [PublicController::class, 'show'])->name('public.show');
        Route::get('{contest}/inscrever', [PublicController::class, 'register'])->name('public.register');

        Route::get('entries/{entry}', [EntryController::class, 'show'])->middleware('auth')->name('entries.show');

    });
});
