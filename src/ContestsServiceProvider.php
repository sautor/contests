<?php

namespace Sautor\Contests;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;
use Sautor\Contests\Livewire\RegisterForm;
use Sautor\Contests\Models\Contest;
use Sautor\Contests\Policies\ContestsPolicy;
use Sautor\Core\Models\Grupo;
use Sautor\Core\Services\Addons\Addon;

class ContestsServiceProvider extends ServiceProvider
{
    protected $policies = [
        Contest::class => ContestsPolicy::class,
        // TestimonialCollection::class => CollectionsPolicy::class,
        // Testimonial::class => TestimonialsPolicy::class,
    ];

    private Addon $addon;

    /**
     * PaymentsServiceProvider constructor.
     *s
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     */
    public function __construct($app)
    {
        parent::__construct($app);
        $this->addon = Addon::create('contests', 'Concursos', 'award', 'Extensão para inscrição e votação para concursos.')
            ->setGroupAddon(true)
            ->setManagersOnly(false)
            ->setEntryRouteForGroup('contests.public.index')
            ->setEntryRouteForGroupAdmin('contests.contests.index')
            ->setRestrict(function (?Grupo $grupo) {
                return ! Contest::where('grupo_id', $grupo->id)->open()->orWhere(function ($query) {
                    $query->votingOpen();
                })->exists();
            })
            ->withAssets();
    }

    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'contests');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');

        $this->registerPolicies();

        $addonService = $this->app->make('AddonService');
        $addonService->register($this->addon);

        Livewire::component('register-form', RegisterForm::class);
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        //
    }

    public function registerPolicies()
    {
        foreach ($this->policies as $key => $value) {
            Gate::policy($key, $value);
        }
    }
}
