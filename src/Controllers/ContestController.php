<?php

namespace Sautor\Contests\Controllers;

use Filament\Notifications\Notification;
use Illuminate\Http\Request;
use Sautor\Contests\Models\Contest;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\Grupo;

class ContestController extends Controller
{
    public function index(Grupo $grupo)
    {
        $this->authorize('update', $grupo);
        $this->authorize('viewAny', Contest::class);
        $contests = Contest::where('grupo_id', $grupo->id)->paginate(20);

        return view('contests::contests.index', compact('grupo', 'contests'));
    }

    public function create(Grupo $grupo)
    {
        $this->authorize('update', $grupo);
        $this->authorize('create', Contest::class);

        return view('contests::contests.create', compact('grupo'));
    }

    public function store(Request $request, Grupo $grupo)
    {
        $this->authorize('update', $grupo);
        $this->authorize('create', Contest::class);

        $this->validate($request, [
            'name' => 'required',
            'open_at' => 'date|nullable',
            'close_at' => 'date|nullable',
            'voting_open_at' => 'date|nullable',
            'voting_close_at' => 'date|nullable',
            'opening_text' => 'nullable',
            'disclaimer' => 'nullable',
            'thankyou_text' => 'nullable',
            'accept_text' => 'boolean|nullable',
            'accept_images' => 'boolean|nullable',
            'show_at_home' => 'boolean|nullable',
            'banner' => 'file|image|nullable',
        ]);

        $contest = new Contest($request->except(['banner', 'accept_text', 'accept_images', 'show_at_home']));
        $contest->accept_text = $request->has('accept_text');
        $contest->accept_images = $request->has('accept_images');
        $contest->show_at_home = $request->has('show_at_home');
        $contest->grupo_id = $grupo->id;
        $contest->save();

        if ($request->hasFile('banner')) {
            if ($request->file('banner')->isValid()) {
                $contest->addMediaFromRequest('banner')->toMediaCollection('banner');
            }
        }

        Notification::make()
            ->title('Concurso criado com sucesso.')
            ->success()
            ->send();

        return redirect($grupo->route('contests.contests.index'));
    }

    public function show(Grupo $grupo, Contest $contest)
    {
        $this->authorize('view', $contest);
        $entries = $contest->entries()->paginate(20);
        $contest->loadCount('entries');

        return view('contests::contests.show', compact('grupo', 'contest', 'entries'));
    }

    public function edit(Grupo $grupo, Contest $contest)
    {
        $this->authorize('update', $contest);

        return view('contests::contests.edit', compact('grupo', 'contest'));
    }

    public function update(Request $request, Grupo $grupo, Contest $contest)
    {
        $this->authorize('update', $contest);
        $this->validate($request, [
            'name' => 'required',
            'open_at' => 'date|nullable',
            'close_at' => 'date|nullable',
            'voting_open_at' => 'date|nullable',
            'voting_close_at' => 'date|nullable',
            'opening_text' => 'nullable',
            'disclaimer' => 'nullable',
            'thankyou_text' => 'nullable',
            'accept_text' => 'boolean|nullable',
            'accept_images' => 'boolean|nullable',
            'show_at_home' => 'boolean|nullable',
            'banner' => 'file|image|nullable',
        ]);

        $contest->fill($request->except(['banner', 'accept_text', 'accept_images', 'show_at_home']));

        $contest->accept_text = $request->has('accept_text');
        $contest->accept_images = $request->has('accept_images');
        $contest->show_at_home = $request->has('show_at_home');
        $contest->save();

        if ($request->hasFile('banner')) {
            if ($request->file('banner')->isValid()) {
                $contest->addMediaFromRequest('banner')->toMediaCollection('banner');
            }
        }

        Notification::make()
            ->title('Concurso editado com sucesso.')
            ->success()
            ->send();

        return redirect($grupo->route('contests.contests.index'));
    }

    public function destroy(Grupo $grupo, Contest $contest)
    {
        $this->authorize('delete', $contest);

        $contest->delete();

        Notification::make()
            ->title('Concurso eliminado com sucesso.')
            ->success()
            ->send();

        return redirect($grupo->route('contests.contests.index'));
    }
}
