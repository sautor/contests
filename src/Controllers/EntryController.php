<?php

namespace Sautor\Contests\Controllers;

use Sautor\Contests\Models\Entry;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\Grupo;

class EntryController extends Controller
{
    public function show(Grupo $grupo, Entry $entry)
    {
        $this->authorize('view', $entry);
        return view('contests::entries.show', compact('grupo', 'entry'));
    }
}
