<?php

namespace Sautor\Contests\Controllers;

use Sautor\Contests\Models\Contest;
use Sautor\Core\Http\Controllers\Controller;
use Sautor\Core\Models\Grupo;

class PublicController extends Controller
{
    public function index(Grupo $grupo)
    {
        $registration_contests = Contest::where('grupo_id', $grupo->id)->open()->get();
        $voting_contests = Contest::where('grupo_id', $grupo->id)->votingOpen()->get();

        if ($voting_contests->isEmpty() && $registration_contests->count() === 1) {
            return redirect($grupo->route('contests.public.show', $registration_contests->first()));
        }

        if ($registration_contests->isEmpty() && $voting_contests->count() === 1) {
            return redirect($grupo->route('contests.public.show', $registration_contests->first()));
        }

        return view('contests::public.index', compact('grupo', 'registration_contests', 'voting_contests'));
    }

    public function show(Grupo $grupo, Contest $contest)
    {
        return view('contests::public.show', compact('grupo', 'contest'));
    }

    public function register(Grupo $grupo, Contest $contest)
    {
        return view('contests::public.register', compact('grupo', 'contest'));
    }
}
