<?php

namespace Sautor\Contests\Livewire;

use Filament\Notifications\Notification;
use Livewire\Component;
use Livewire\WithFileUploads;
use Sautor\Contests\Models\Contest;
use Sautor\Contests\Models\Entry;

class RegisterForm extends Component
{
    use WithFileUploads;

    public Contest $contest;

    public $name;

    public $origin;

    public $content;

    public $images = [];

    public $consent;

    public $isSaved = false;

    public function save()
    {
        $rules = [
            'name' => 'required',
            'consent' => 'accepted',
            'images.*' => 'image',
        ];

        if ($this->contest->accept_text && $this->contest->accept_images) {
            $rules['content'] = 'required_without:images';
            $rules['images'] = 'required_without:content';
        } elseif ($this->contest->accept_text) {
            $rules['content'] = 'required';
        } elseif ($this->contest->accept_images) {
            $rules['images'] = 'required';
        }

        $this->validate($rules, [], [
            'name' => 'Nome',
            'content' => 'Conteúdo',
            'images' => 'Imagens',
            'consent' => 'Consentimento',
        ]);

        try {
            \DB::transaction(function () {
                $entry = Entry::create([
                    'name' => $this->name,
                    'origin' => $this->origin,
                    'content' => $this->content,
                    'contest_id' => $this->contest->id,
                    'ip' => request()->ip(),
                    'pessoa_id' => \Auth::id(),
                ]);

                foreach ($this->images as $image) {
                    $entry->addMedia($image)->toMediaCollection('images');
                }

                $this->isSaved = true;
            });
        } catch (\Throwable $e) {
            throw $e;
            Notification::make()
                ->title('Ocorreu um erro.')
                ->body('Por favor tente novamente.')
                ->danger()
                ->send();
        }
    }

    public function render()
    {
        return view('contests::livewire.register-form');
    }
}
