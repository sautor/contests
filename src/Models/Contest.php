<?php

namespace Sautor\Contests\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mtvs\EloquentHashids\HasHashid;
use Mtvs\EloquentHashids\HashidRouting;
use Sautor\Core\Models\Grupo;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Contest extends Model implements HasMedia
{
    use HasHashid, HashidRouting;
    use InteractsWithMedia;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cts_contests';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'open_at' => 'datetime',
        'close_at' => 'datetime',
        'voting_open_at' => 'datetime',
        'voting_close_at' => 'datetime',
        'accept_text' => 'boolean',
        'accept_images' => 'boolean',
    ];

    protected function isOpen(): Attribute
    {
        return Attribute::make(
            get: fn () => ($this->open_at && $this->open_at < Carbon::now()) && (! $this->close_at || $this->close_at > Carbon::now())
        );
    }

    protected function isClosed(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->close_at && $this->close_at < Carbon::now()
        );
    }

    protected function isVotingOpen(): Attribute
    {
        return Attribute::make(
            get: fn () => ($this->voting_open_at && $this->voting_open_at < Carbon::now()) && (! $this->voting_close_at || $this->voting_close_at > Carbon::now())
        );
    }

    protected function isVotingClosed(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->voting_close_at && $this->voting_close_at < Carbon::now()
        );
    }

    public function grupo(): BelongsTo
    {
        return $this->belongsTo(Grupo::class);
    }

    public function entries(): HasMany
    {
        return $this->hasMany(Entry::class);
    }

    public function scopeOpen(Builder $query): void
    {
        $query->where('open_at', '<', Carbon::now())->where(function (Builder $qb) {
            $qb->whereNull('close_at')->orWhere('close_at', '>', Carbon::now());
        });
    }

    public function scopeClosed(Builder $query): void
    {
        $query->where('close_at', '<', Carbon::now());
    }

    public function scopeVotingOpen(Builder $query): void
    {
        $query->where('voting_open_at', '<', Carbon::now())->where(function (Builder $qb) {
            $qb->whereNull('voting_close_at')->orWhere('voting_close_at', '>', Carbon::now());
        });
    }

    public function scopeVotingClosed(Builder $query): void
    {
        $query->where('voting_close_at', '<', Carbon::now());
    }

    public function scopeShowAtHome(Builder $query): void
    {
        $query->where('show_at_home', true);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('banner')->singleFile();
    }
}
