<?php

namespace Sautor\Contests\Policies;

use Carbon\Carbon;
use Illuminate\Auth\Access\HandlesAuthorization;
use Sautor\Contests\Models\Contest;
use Sautor\Core\Models\Pessoa;

class ContestsPolicy
{
    use HandlesAuthorization;

    public function viewAny(?Pessoa $user)
    {
        return true;
    }

    public function view(?Pessoa $user, Contest $contest)
    {
        return ($contest->open_at && $contest->open_at->isBefore(Carbon::now())) || $contest->grupo->isManagedBy($user);
    }

    public function create(Pessoa $user)
    {
        return $user->grupos()->where('inscricoes.responsavel', true)->exists();
    }

    public function update(Pessoa $user, Contest $contest)
    {
        return $contest->grupo->isManagedBy($user);
    }

    public function delete(Pessoa $user, Contest $contest)
    {
        return $contest->grupo->isManagedBy($user);
    }

    public function restore(Pessoa $user, Contest $contest)
    {
        return $contest->grupo->isManagedBy($user);
    }

    public function forceDelete(Pessoa $user, Contest $contest)
    {
        return false;
    }
}
