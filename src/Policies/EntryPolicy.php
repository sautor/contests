<?php

namespace Sautor\Contests\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Sautor\Contests\Models\Entry;
use Sautor\Core\Models\Pessoa;

class EntryPolicy
{
    use HandlesAuthorization;

    public function viewAny(Pessoa $user)
    {
        //
    }

    public function view(Pessoa $user, Entry $entry): bool
    {
        return $user->can('view', $entry->contest);
    }

    public function create(?Pessoa $user): bool
    {
        return true;
    }

    public function update(Pessoa $user, Entry $entry): bool
    {
        return $user->can('update', $entry->contest);
    }

    public function delete(Pessoa $user, Entry $entry): bool
    {
        return $user->can('update', $entry->contest);

    }

    public function restore(Pessoa $user, Entry $entry): bool
    {
        return $user->can('update', $entry->contest);

    }

    public function forceDelete(Pessoa $user, Entry $entry): bool
    {
        return false;
    }
}
