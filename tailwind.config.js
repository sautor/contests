import preset from './vendor/sautor/core/tailwind.preset.js'

export default {
    presets: [preset],
    prefix: 's-cts-'
}
